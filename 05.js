var fs    = require('fs')
var path  = require('path')

var dirName   = process.argv[2]
var extFilter = process.argv[3]

var readDirCb = function(err, filenames) {
  if (err) return;

  function filterByExt(filename) {

    var extName = path.extname(filename);

    if (extName.slice(1, extName.length) === extFilter) {
      console.log(filename);
    }
  }

  filenames.forEach(filterByExt);
}

fs.readdir(dirName, readDirCb);
